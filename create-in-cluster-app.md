CLI

```
argocd app create --name test \
--repo https://gitlab.com/k8s_freddie.entity/argocd_gs \
--dest-server https://kubernetes.default.svc \
--dest-namespace dev --path dev
```

ARGOCD

```
k create namespace argocd
k create ns dev
k apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
k apply -f app.yaml
k get svc -n argocd
k port-forward -n argocd svc/argocd-server 8080:443
k -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
k -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" then browser console type atob('<valueFromLeft>')
```

#CI
Push everything to Gitlab main branch
